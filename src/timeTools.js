// import assert from "assert-plus";
// import assert from "assert";
// const assert = require("assert").strict;

export function timeStr(time, millis = true) {
  const timeToSeconds = millis ? 1000 : 1;
  const absSeconds = Math.floor(time / timeToSeconds);
  const seconds = absSeconds % 60;
  const absMinutes = Math.floor(absSeconds / 60);
  const minutes = absMinutes % 60;
  const hours = Math.floor(absMinutes / 60);
  return `${hours.toString().padStart(2, "0")}:${minutes
    .toString()
    .padStart(2, "0")}:${seconds.toString().padStart(2, "0")}`;
}
